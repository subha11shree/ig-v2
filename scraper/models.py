from django.db import models
from django.utils import timezone
from django.urls import reverse

OK_CHOICES = (('not ok', 'not ok'),('ok', 'ok'))

class IgComments(models.Model):
    entity_id = models.IntegerField(primary_key=True)
    profile_id = models.CharField(max_length=64)
    post_shortcode = models.CharField(max_length=25)
    commenter_name = models.CharField(max_length=128)
    commenter_id = models.BigIntegerField()
    fan_relationship = models.CharField(max_length=64)
    message = models.CharField(max_length=4096, blank=True, null=True)
    comment_id = models.BigIntegerField()
    created_time = models.IntegerField(blank=True, null=True)
    fetch_time = models.DateTimeField(blank=True, null=True)
    polarity = models.SmallIntegerField(blank=True, null=True)
    subjectivity = models.SmallIntegerField(blank=True, null=True)
    commenter_id_hash = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ig_comments'
        unique_together = (('entity_id', 'profile_id', 'post_shortcode', 'comment_id'),)


class IgEntities(models.Model):
    entity_id = models.PositiveIntegerField(primary_key=True)
    category = models.CharField(max_length=3)
    profile_id = models.CharField(unique=True, max_length=64)
    num_http_reqs = models.PositiveIntegerField(blank=True, null=True)
    num_posts = models.PositiveIntegerField(blank=True, null=True)
    status = models.CharField(max_length=8)
    crawl_code = models.CharField(max_length=32, blank=True, null=True)
    crawl_hostname = models.CharField(max_length=32, blank=True, null=True)
    crawl_start_time = models.DateTimeField(blank=True, null=True)
    crawl_end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ig_entities'


class IgLikes(models.Model):
    entity_id = models.IntegerField(primary_key=True)
    profile_id = models.CharField(max_length=64)
    post_shortcode = models.CharField(max_length=25)
    liker_name = models.CharField(max_length=128)
    liker_id = models.BigIntegerField()
    fan_relationship = models.CharField(max_length=64)
    post_datetime = models.IntegerField(blank=True, null=True)
    fetch_time = models.DateTimeField(blank=True, null=True)
    polarity = models.SmallIntegerField(blank=True, null=True)
    subjectivity = models.SmallIntegerField(blank=True, null=True)
    liker_id_hash = models.CharField(max_length=128, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ig_likes'
        unique_together = (('entity_id', 'profile_id', 'post_shortcode', 'liker_id'),)


class IgPosts(models.Model):
    entity_id = models.IntegerField(primary_key=True)
    profile_id = models.CharField(max_length=64)
    post_shortcode = models.CharField(max_length=25)
    post_id = models.CharField(max_length=64)
    post_url = models.CharField(max_length=255, blank=True, null=True)
    post_text = models.CharField(max_length=4096, blank=True, null=True)
    post_type = models.CharField(max_length=20, blank=True, null=True)
    post_datetime = models.IntegerField(blank=True, null=True)
    total_comments = models.IntegerField(blank=True, null=True)
    captured_comments = models.IntegerField(blank=True, null=True)
    total_likes = models.IntegerField(blank=True, null=True)
    captured_likes = models.IntegerField(blank=True, null=True)
    status = models.CharField(max_length=8)
    crawl_code = models.CharField(max_length=32, blank=True, null=True)
    crawl_hostname = models.CharField(max_length=32, blank=True, null=True)
    crawl_start_time = models.DateTimeField(blank=True, null=True)
    crawl_end_time = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ig_posts'
        unique_together = (('entity_id', 'profile_id', 'post_shortcode'),)
