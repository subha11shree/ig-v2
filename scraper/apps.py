from django.apps import AppConfig
import django

class ScraperConfig(AppConfig):
    name = 'scraper'
    django.setup()
