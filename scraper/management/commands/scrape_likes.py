from django.core.management.base import BaseCommand, CommandError
import json
import re
import hashlib
import fs
import typing
import six
import argparse
import os
import csv
import time
import socket

from requests import Session
from six.moves.http_cookiejar import LWPCookieJar, FileCookieJar
from datetime import datetime
from django.utils import timezone
from django.db import transaction
from scraper.models import *

class Command(BaseCommand):
    COOKIE_FILE = 'cookies.txt'
    WAIT_SECONDS_ON_RATE_LIMITED = 1 * 300
    WAIT_SECONDS_ON_ERROR = 1 * 60
    TOTAL_SLEEP_TIME_ON_RATE_LIMITED = 0
    APPNAME='instagram_scraper'
    AUTHOR='TEG'
    VERSION=1
    base_url = 'https://www.instagram.com'
    dump_file = str(os.getcwd()) + '/scraper/dumps/entity_json/{}'

    def create_gis(self, rhx_gis, query_variables):
        h = hashlib.md5("{}:{}".format(rhx_gis, query_variables).encode('utf-8')).hexdigest()
        #print("GIS: "+str(h))
        return h

    def cachefs(self):
        url = "usercache://{}:{}:{}".format(self.APPNAME, self.AUTHOR, self.VERSION)
        return fs.open_fs(url, create=True)

    def init_session(self, session=None):
        session = session or Session()
        session.cookies = LWPCookieJar(self.cachefs().getsyspath(self.COOKIE_FILE))
        try:
            typing.cast(FileCookieJar, session.cookies).load()
        except IOError:
            pass
        typing.cast(FileCookieJar, session.cookies).clear_expired_cookies()
        return session

    def handle(self, *args, **options):
        self.session = self.init_session(Session())
        self.run_datetime = timezone.now()
        print("New Script")
        self.process_posts()

    def process_posts(self):
        start_time = time.time()
        while True:
            with transaction.atomic():
                posts_list = IgPosts.objects.select_for_update().filter(status='new')
                post = posts_list.first()
                if post is None:
                    print("Total Sleep Time %s" % self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED)
                    print("--- %s seconds ---" % (time.time() - start_time))
                    print("All Entities are crawled")
                    return
                profile_id = post.profile_id
                entity_id = post.entity_id
                post_created_time = post.post_datetime
                post_shortcode = post.post_shortcode
                post_type = post.post_type
                print("Collecting post info for : "+profile_id+" Post : "+post_shortcode)
                post.status = 'crawling'
                post.crawl_hostname = socket.gethostname()
                post.crawl_start_time = timezone.now()
                post.save()
            total_comments, captured_comments, total_likes, captured_likes, crawl_code = self.process_post(profile_id, entity_id, post_created_time, post_shortcode, post_type)
            post.total_comments = total_comments
            post.captured_comments = captured_comments
            post.total_likes = total_likes
            post.captured_likes = captured_likes
            post.crawl_code = crawl_code
            post.crawl_end_time = timezone.now()
            post.status = 'crawled'
            post.save()

    def process_post(self, profile_id, entity_id, post_created_time, post_shortcode, post_type):
        total_comments = captured_comments = total_likes = captured_likes = 0
        url = "{}/{}/{}".format(self.base_url, 'p', post_shortcode)
        #print("Collecting post info for : "+profile_id+" Post URL: "+url)
        while True:
            try:
                post_page_source = self.session.get(url)
            except Exception as e:
                #error = Entitymediaerror(
                    #run_datetime = self.run_datetime,
                    #entity = profile_id,
                    #error = e,
                    #status = 'not ok',
                    #created_at = timezone.now(),
                    #)
                #error.save()
                print("Post Page Source Error - Sleep now for : "+ str(self.WAIT_SECONDS_ON_ERROR) + "sec")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue
            #print(post_page_source)
            post_status = post_page_source.status_code
            #print("Post Status : "+str(post_page_source.status_code))
            if post_status == 404:
                return total_comments, captured_comments, total_likes, captured_likes, post_status

            if post_status != 200:
                #error = Entitymediaerror(
                    #run_datetime = self.run_datetime,
                    #entity = profile_id,
                    #error = 'HTTP status: ' +str(post_page_source.status_code),
                    #response = 'HTTP status: ' +str(post_page_source.status_code),
                    #status = 'not ok',
                    #created_at = timezone.now(),
                    #)
                #error.save()
                print("Post Page Status Code : " + str(post_status) + "So Sleep for : " + str(self.WAIT_SECONDS_ON_ERROR) + "secs")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue

            try:
                postContent = post_page_source.text
                postInfo=re.search('<script[^>]+>window\._sharedData\s+=\s(.*?);<\/script>', postContent)
                postJSON = json.loads(postInfo.group(1)) if postInfo else None
                with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"_post_content")), 'w') as fp:
                    fp.write(postInfo.group(1))
            except Exception as e:
                print("Post Page Doesnot contain required JS. So Sleep for : " + str(self.WAIT_SECONDS_ON_ERROR) + "secs")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue
            break

        def get_first_likes_params():
            return {
                "shortcode" : post_shortcode,
                "include_reel":"false",
                "first":50
                }
        post_rhx_gis = postJSON.get('rhx_gis')
        likes_query_url = "{}{}".format(self.base_url, '/graphql/query/?query_hash={}&variables={{}}'.format('e0f59e4a1c8d78d0161873bc2ee7ec44'))
        likes_params = get_first_likes_params()
        json_likes_params = json.dumps(likes_params, separators=(',', ':'))
        likes_signature = self.create_gis(post_rhx_gis, json_likes_params)
        self.session.headers['x-instagram-gis'] = likes_signature
        likes_url = likes_query_url.format(json_likes_params)
        likes_content = self.session.get(likes_url)
        with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"likes")), 'w') as fp:
            fp.write(likes_content.text)
        likes_data = likes_content.json()
        likes = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('edges')
        has_next_page = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('page_info').get('has_next_page')
        cursor = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('page_info').get('end_cursor')

        def get_likes_params(cursor):
            return {
                "shortcode" : post_shortcode,
                "include_reel" : "false",
                "first" : 50,
                "after" : cursor
                }

        captured_likes = 0
        likes_list = []
        while True:
            if len(likes_list) > 10000:
                self.process_likes(likes_list, entity_id, post_shortcode, profile_id, post_type, post_created_time)
                likes_list = []
            for like in likes:
                node = like.get('node')
                like_id = node.get('id')
                if not any(node['id'] == like_id for node in likes_list):
                    captured_likes += 1
                    likes_list.append(node)
            print("Current collected Likes count : "+ str(captured_likes))
            if has_next_page:
                likes_query_url = "{}{}".format(self.base_url, '/graphql/query/?query_hash={}&variables={{}}'.format('e0f59e4a1c8d78d0161873bc2ee7ec44'))
                likes_params = get_likes_params(cursor)
                json_likes_params = json.dumps(likes_params, separators=(',', ':'))
                likes_signature = self.create_gis(post_rhx_gis, json_likes_params)
                self.session.headers['x-instagram-gis'] = likes_signature
                likes_url = likes_query_url.format(json_likes_params)
                while True:
                    likes_content = self.session.get(likes_url)
                    try:
                        with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"likes"+str(captured_likes))), 'w') as fp:
                            fp.write(likes_content.text)
                        likes_data = likes_content.json()
                        likes = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('edges')
                        has_next_page = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('page_info').get('has_next_page')
                        cursor = likes_data.get('data').get('shortcode_media').get('edge_liked_by').get('page_info').get('end_cursor')
                        #print("CURSOR : "+cursor)
                    except Exception as e:
                        #error = Entitymediaerror(
                            #run_datetime = self.run_datetime,
                            #entity = profile_id,
                            #error = e,
                            #status = 'not ok',
                            #created_at = timezone.now(),
                            #)
                            #error.save()
                        print("Failed while processing data")
                        #print(likes_data)
                        if likes_data.get('status').lower() != 'ok':
                            #error = Entitymediaerror(
                                #run_datetime = self.run_datetime,
                                #entity = profile_id,
                                #response = json.dumps(likes_data),
                                #status = 'not ok',
                                #created_at = timezone.now(),
                                #)
                            #error.save()
                            print(likes_data.get('message'))
                        if likes_data.get('message') == 'rate limited':
                            print("Rate Limited. Sleeping " + str(self.WAIT_SECONDS_ON_RATE_LIMITED) + ' seconds')
                            time.sleep(self.WAIT_SECONDS_ON_RATE_LIMITED)
                            self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_RATE_LIMITED
                            print("Slept now for : "+ str(self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED) + "sec")
                            continue
                        elif likes_data.get('message') == 'Please wait a few minutes before you try again.':
                            print("Request to Sleep. Sleeping 60 seconds")
                            time.sleep(60)
                            continue
                    break
            else:
                self.process_likes(likes_list, entity_id, post_shortcode, profile_id, post_type, post_created_time)
                return 0, 0, 0, captured_likes, 200


    def process_likes(self,likes, entity_id, post_shortcode, profile_id, post_type, post_created_time):
        if post_type == 'Picture':
            fan_relationship = 'instagram_likers'
        else:
            fan_relationship = 'instagram_video_likers'

        def comment_from_node(comment_node, fan_relationship):
            #node = comment_node.get('node')
            user_id = comment_node.get('id')
            user_name = comment_node.get('username')
            like = IgLikes(
                entity_id = entity_id,
                post_shortcode = post_shortcode,
                profile_id = profile_id,
                liker_name = user_name,
                liker_id = user_id,
                fan_relationship = fan_relationship,
                fetch_time = timezone.now(),
                post_datetime = post_created_time
            )
            return like

        comments_and_likes = [
            comment_from_node(like, fan_relationship) for like in likes
        ]
        IgLikes.objects.bulk_create(comments_and_likes)
