from django.core.management.base import BaseCommand, CommandError
import json
import re
import hashlib
import fs
import typing
import six
import argparse
import os
import csv
import time
import socket

from requests import Session
from six.moves.http_cookiejar import LWPCookieJar, FileCookieJar
from datetime import datetime
from django.utils import timezone
from django.db import transaction
from scraper.models import *

class Command(BaseCommand):
    COOKIE_FILE = 'cookies.txt'
    WAIT_SECONDS_ON_RATE_LIMITED = 1 * 300
    WAIT_SECONDS_ON_ERROR = 1 * 60
    TOTAL_SLEEP_TIME_ON_RATE_LIMITED = 0
    APPNAME='instagram_scraper'
    AUTHOR='TEG'
    VERSION=1
    base_url = 'https://www.instagram.com'
    dump_file = str(os.getcwd()) + '/scraper/dumps/entity_json/{}'

    def create_gis(self, rhx_gis, query_variables):
        h = hashlib.md5("{}:{}".format(rhx_gis, query_variables).encode('utf-8')).hexdigest()
        #print("GIS: "+str(h))
        return h

    def cachefs(self):
        url = "usercache://{}:{}:{}".format(self.APPNAME, self.AUTHOR, self.VERSION)
        return fs.open_fs(url, create=True)

    def init_session(self, session=None):
        session = session or Session()
        session.cookies = LWPCookieJar(self.cachefs().getsyspath(self.COOKIE_FILE))
        try:
            typing.cast(FileCookieJar, session.cookies).load()
        except IOError:
            pass
        typing.cast(FileCookieJar, session.cookies).clear_expired_cookies()
        return session

    def handle(self, *args, **options):
        self.session = self.init_session(Session())
        self.run_datetime = timezone.now()
        print("New Script")
        self.process_posts()

    def process_posts(self):
        start_time = time.time()
        while True:
            with transaction.atomic():
                posts_list = IgPosts.objects.filter(status='new').select_for_update(skip_locked=True)
                post = post_list.first()
                if post is None:
                    print("Total Sleep Time %s" % self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED)
                    print("--- %s seconds ---" % (time.time() - start_time))
                    print("All Entities are crawled")
                    return
                profile_id = post.profile_id
                entity_id = post.entity_id
                post_created_time = post.post_datetime
                post_shortcode = post.post_shortcode
                post_type = post.post_type
                print("Collecting post info for : "+profile_id+" Post : "+post_shortcode)
                post.status = 'crawling'
                post.crawl_hostname = socket.gethostname()
                post.crawl_start_time = timezone.now()
                post.save()
            total_comments, captured_comments, total_likes, captured_likes, crawl_code = self.process_post(profile_id, entity_id, post_created_time, post_shortcode, post_type)
            post.total_comments = total_comments
            post.captured_comments = captured_comments
            post.total_likes = total_likes
            post.captured_likes = captured_likes
            post.crawl_code = crawl_code
            post.crawl_end_time = timezone.now()
            post.status = 'crawled'
            post.save()

    def process_post(self, profile_id, entity_id, post_created_time, post_shortcode, post_type):
        total_comments = captured_comments = total_likes = captured_likes = 0
        url = "{}/{}/{}".format(self.base_url, 'p', post_shortcode)
        #print("Collecting post info for : "+profile_id+" Post URL: "+url)
        while True:
            try:
                post_page_source = self.session.get(url)
            except Exception as e:
                #error = Entitymediaerror(
                    #run_datetime = self.run_datetime,
                    #entity = profile_id,
                    #error = e,
                    #status = 'not ok',
                    #created_at = timezone.now(),
                    #)
                #error.save()
                print("Post Page Source Error - Sleep now for : "+ str(self.WAIT_SECONDS_ON_ERROR) + "sec")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue
            #print(post_page_source)
            post_status = post_page_source.status_code
            #print("Post Status : "+str(post_page_source.status_code))
            if post_status == 404:
                return total_comments, captured_comments, total_likes, captured_likes, post_status

            if post_status != 200:
                #error = Entitymediaerror(
                    #run_datetime = self.run_datetime,
                    #entity = profile_id,
                    #error = 'HTTP status: ' +str(post_page_source.status_code),
                    #response = 'HTTP status: ' +str(post_page_source.status_code),
                    #status = 'not ok',
                    #created_at = timezone.now(),
                    #)
                #error.save()
                print("Post Page Status Code : " + str(post_status) + "So Sleep for : " + str(self.WAIT_SECONDS_ON_ERROR) + "secs")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue

            try:
                postContent = post_page_source.text
                postInfo=re.search('<script[^>]+>window\._sharedData\s+=\s(.*?);<\/script>', postContent)
                postJSON = json.loads(postInfo.group(1)) if postInfo else None
                with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"_post_content")), 'w') as fp:
                    fp.write(postInfo.group(1))
            except Exception as e:
                print("Post Page Doesnot contain required JS. So Sleep for : " + str(self.WAIT_SECONDS_ON_ERROR) + "secs")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                self.WAIT_SECONDS_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_ERROR
                continue
            break

        total_likes = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_preview_like').get('count')
        likes = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_preview_like').get('edges')
        captured_likes = len(likes)
        #likers information collection
        if captured_likes != 0:
            self.process_likes(likes, entity_id, post_shortcode, profile_id, post_type, post_created_time)
        else:
            print("Likes count ZERO")

        def get_comments_params(cursor):
            return {
                'shortcode': post_shortcode,
                'first': 50,
                'after': cursor
            }

        #commentors information collection
        captured_comments = 0
        comments_list = []
        comments_disabled = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('comments_disabled')
        if comments_disabled:
            return total_comments, captured_comments, total_likes, captured_likes, post_status
        total_comments = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_to_comment').get('count')
        print("Entity Crawling : "+profile_id)
        print("Comments count specified : "+str(total_comments))
        if total_comments != 0:
            comments = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_to_comment').get('edges')
            if not comments:
                return total_comments, captured_comments, total_likes, captured_likes, post_status
            has_next_page = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_to_comment').get('page_info').get('has_next_page')
            cursor = postJSON.get('entry_data').get('PostPage')[0].get('graphql').get('shortcode_media').get('edge_media_to_comment').get('page_info').get('end_cursor')
            post_rhx_gis = postJSON.get('rhx_gis')
            while True:
                if not comments:
                    if not comments_list:
                        return total_comments, captured_comments, total_likes, captured_likes, post_status
                    else:
                        self.process_comments(comments_list, entity_id, post_shortcode, profile_id, post_type)
                        return total_comments, captured_comments, total_likes, captured_likes, post_status
                first_comment_time = comments[0].get('node').get('created_at')
                last_comment_time = comments[len(comments)-1].get('node').get('created_at')
                if len(comments_list) > 10000:
                    self.process_comments(comments_list, entity_id, post_shortcode, profile_id, post_type)
                    comments_list = []
                if first_comment_time <= post_created_time + 259200 or last_comment_time <= post_created_time + 259200:
                    for comment in comments:
                        comments_dict = {}
                        node = comment.get('node')
                        comment_timestamp = node.get('created_at')
                        if comment_timestamp <= post_created_time + 259200:
                            comment_id = node.get('id')
                            if not any(node['id'] == comment_id for node in comments_list):
                                captured_comments += 1
                                comments_list.append(node)
                        else:
                            continue
                    print("Current collected comments count : "+ str(captured_comments))
                    if total_comments <= 40 or not has_next_page:
                        print("Collected Comments count : "+ str(captured_comments))
                        self.process_comments(comments_list, entity_id, post_shortcode, profile_id, post_type)
                        return total_comments, captured_comments, total_likes, captured_likes, post_status
                elif not has_next_page:
                    return total_comments, captured_comments, total_likes, captured_likes, post_status

                comments_query_url = "{}{}".format(self.base_url, '/graphql/query/?query_hash={}&variables={{}}'.format('33ba35852cb50da46f5b5e889df7d159'))
                comments_params = get_comments_params(cursor)
                json_comments_params = json.dumps(comments_params, separators=(',', ':'))
                comments_signature = self.create_gis(post_rhx_gis, json_comments_params)
                self.session.headers['x-instagram-gis'] = comments_signature
                comments_url = comments_query_url.format(json_comments_params)
                while True:
                    comments_content = self.session.get(comments_url)
                    try:
                        #with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"comments"+str(captured_comments))), 'w') as fp:
                            #fp.write(comments_content.text)
                        comments_data = comments_content.json()
                        comments = comments_data.get('data').get('shortcode_media').get('edge_media_to_comment').get('edges')
                        has_next_page = comments_data.get('data').get('shortcode_media').get('edge_media_to_comment').get('page_info').get('has_next_page')
                        cursor = comments_data.get('data').get('shortcode_media').get('edge_media_to_comment').get('page_info').get('end_cursor')
                        #print("CURSOR : "+cursor)
                    except Exception as e:
                        #error = Entitymediaerror(
                            #run_datetime = self.run_datetime,
                            #entity = profile_id,
                            #error = e,
                            #status = 'not ok',
                            #created_at = timezone.now(),
                            #)
                            #error.save()
                        print("Failed while processing data")
                        #print(comments_data)
                        if comments_data.get('status').lower() != 'ok':
                            #error = Entitymediaerror(
                                #run_datetime = self.run_datetime,
                                #entity = profile_id,
                                #response = json.dumps(comments_data),
                                #status = 'not ok',
                                #created_at = timezone.now(),
                                #)
                            #error.save()
                            print(comments_data.get('message'))
                        if comments_data.get('message') == 'rate limited':
                            print("Rate Limited. Sleeping " + str(self.WAIT_SECONDS_ON_RATE_LIMITED) + ' seconds')
                            time.sleep(self.WAIT_SECONDS_ON_RATE_LIMITED)
                            self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED += self.WAIT_SECONDS_ON_RATE_LIMITED
                            print("Slept now for : "+ str(self.TOTAL_SLEEP_TIME_ON_RATE_LIMITED) + "sec")
                            continue
                        elif comments_data.get('message') == 'Please wait a few minutes before you try again.':
                            print("Request to Sleep. Sleeping 60 seconds")
                            time.sleep(60)
                            continue
                    break
        else:
            return total_comments, captured_comments, total_likes, captured_likes, post_status

    def process_likes(self,likes, entity_id, post_shortcode, profile_id, post_type, post_created_time):
        if post_type == 'Picture':
            fan_relationship = 'instagram_likers'
        else:
            fan_relationship = 'instagram_video_likers'

        for like in likes:
            node = like.get('node')
            liker_id = node.get('id')
            liker_name = node.get('username')
            #print(liker_name)
            likes_media = IgLikes(
                entity_id = entity_id,
                post_shortcode = post_shortcode,
                profile_id = profile_id,
                liker_name = liker_name,
                liker_id = liker_id,
                fan_relationship = fan_relationship,
                fetch_time = timezone.now(),
                post_datetime = post_created_time
                )
            likes_media.save()

    def process_comments(self, comments, entity_id, post_shortcode, profile_id, post_type):
        #print(comments)
        with open(self.dump_file.format("{}.json".format(profile_id+"_"+post_shortcode+"commentsFinal")), 'w') as fp:
            for item in comments:
                fp.write("%s\n" % item)
        if post_type == 'Picture':
            fan_relationship = 'instagram_commenters'
        else:
            fan_relationship = 'instagram_video_commenters'

        def comment_from_node(comment_node, fan_relationship):
            #node = comment_node.get('node')
            comment_id = comment_node.get('id')
            comment_text = comment_node.get('text')
            commented_time = comment_node.get('created_at')
            user_id = comment_node.get('owner').get('id')
            user_name = comment_node.get('owner').get('username')
            comment = IgComments(
                entity_id = entity_id,
                post_shortcode = post_shortcode,
                profile_id = profile_id,
                commenter_name = user_name,
                commenter_id = user_id,
                fan_relationship = fan_relationship,
                message = comment_text,
                comment_id = comment_id,
                created_time = commented_time,
                fetch_time = timezone.now()
            )
            return comment

        comments_and_likes = [
            comment_from_node(comment, fan_relationship) for comment in comments
        ]
        IgComments.objects.bulk_create(comments_and_likes)
