from django.core.management.base import BaseCommand, CommandError
import json
import re
import hashlib
import fs
import typing
import six
import argparse
import os
import csv
import time
import socket

from requests import Session
from six.moves.http_cookiejar import LWPCookieJar, FileCookieJar
from datetime import datetime
from django.utils import timezone
from django.db import transaction
from scraper.models import *
from dateutil.parser import parse

class Command(BaseCommand):
    COOKIE_FILE = 'cookies.txt'
    APPNAME='instagram_scraper'
    AUTHOR='TEG'
    VERSION=1
    WAIT_SECONDS_ON_RATE_LIMITED = 1 * 120
    WAIT_SECONDS_ON_ERROR = 1 * 60
    dump_file = str(os.getcwd()) + '/scraper/dumps/entity_json/{}'

    base_url = 'https://www.instagram.com'
    user_agent = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0:e3QLGW0hDtHE0fHepuJVOjXVt0uBZ9Y8'

    def create_gis(self, rhx_gis, query_variables):
        h = hashlib.md5("{}:{}".format(rhx_gis, query_variables).encode('utf-8')).hexdigest()
        #print("GIS: "+str(h))
        return h

    def cachefs(self):
        url = "usercache://{}:{}:{}".format(self.APPNAME, self.AUTHOR, self.VERSION)
        return fs.open_fs(url, create=True)

    def init_session(self, session=None):
        session = session or Session()
        session.cookies = LWPCookieJar(self.cachefs().getsyspath(self.COOKIE_FILE))
        try:
            typing.cast(FileCookieJar, session.cookies).load()
        except IOError:
            pass
        typing.cast(FileCookieJar, session.cookies).clear_expired_cookies()
        return session

    def add_arguments(self, parser):
        parser.add_argument('--date', dest='date', required=False, help="Crawl Date")
        #parser.add_argument('--entity', dest='entity', required=False, help="Entity To be crawled")
        #parser.add_argument('--entity_id', dest='entity_id', required=False, help="ID of that Entity To be crawled")

    def handle(self, *args, **options):
        self.session = self.init_session(Session())
        self.run_datetime = timezone.now()
        crawlDate = options['date']
        #profile_id = options['entity']
        #entity_id = options['entity_id']
        #self.postsCollection(crawlDate, profile_id, entity_id)
        self.postsCollection(crawlDate)

    #def postsCollection(self, crawlDate, profile_id, entity_id):
    def postsCollection(self, crawlDate):
        date = parse(crawlDate)
        unix_crawl_start_timestamp = date.date().strftime("%s")
        crawl_start_timestamp = int(unix_crawl_start_timestamp)
        crawl_end_timestamp = crawl_start_timestamp + 86400
        print("Crawl Start timestamp : " + str(timezone.make_aware(datetime.fromtimestamp(crawl_start_timestamp))))
        print("Crawl End timestamp : " + str(timezone.make_aware(datetime.fromtimestamp(crawl_end_timestamp))))
        while True:
            profile_id, entity_id = self.getNextEntityToCrawl()
            if entity_id == 0:
                print("All Entities are crawled")
                return
            posts_list, num_http_requests , num_posts, crawl_code  = self.crawlEntityAndCollectPosts(profile_id, crawl_start_timestamp, crawl_end_timestamp)
            print("Number of posts collected : " + str(num_posts))
            self.storePostsToDb(posts_list, profile_id, entity_id)
            self.updateEntitesTable(num_http_requests , num_posts, crawl_code, entity_id)

    def getNextEntityToCrawl(self):
        profile_id = 'null'
        entity_id = 0
        with transaction.atomic():
            entity_list = IgEntities.objects.select_for_update().filter(status='new')[:1]
            if not entity_list.exists():
                return profile_id, entity_id
            entity = entity_list[0]
            profile_id = entity.profile_id
            entity_id = entity.entity_id
            print(profile_id)
            entity.status = 'crawling'
            entity.crawl_start_time = timezone.now()
            entity.crawl_hostname = socket.gethostname()
            entity.save()
        return profile_id, entity_id

    def crawlEntityAndCollectPosts(self, entity, crawl_start_timestamp, crawl_end_timestamp):
        """
        Crawl an Entity and collect all posts within the required time range and return the posts list.
        Inputs : Entity Name (Instagram Profile Name), Crawl Range - crawl_start_time & crawl_end_time
        Steps Involved:
        1. Create Instagram URL using Entity Name
        2. Make URL call and get Entity page Source - First Page
        3. Extract only the required JSON from the Page source
        4. Parse the JSON to collect only the required fields : rhx_gis, profile_id, cursor, has_next_page, post_edges(set of posts in JSON format)
        5. From Set of Posts extract only posts which fit in the reuired time Range
        6. If all the posts for required time range is not collected, continue crawling
        7. Create second page URL
        8. Make URL call and get posts information in JSON format
        9. Parse the JSON and collect posts
        10. If all the posts for required time range is not collected, continue from step - 7
        """
        num_posts = 0
        first_limit = 12
        posts_list = []
        continueCrawl = True
        url = self.createFirstPageUrl(entity)
        print("Posts URL: "+url)
        first_page_source, num_http_requests, page_status = self.getFirstPageSource(url, entity)
        if page_status != 200 or num_http_requests > 10 or first_page_source is None:
            return posts_list, num_http_requests, num_posts, page_status
        first_page_source_json = json.loads(first_page_source.group(1))
        rhx_gis, profile_id, cursor, has_next_page, post_edges = self.parseFirstPageJson(first_page_source_json)
        if len(post_edges) == 0 or profile_id == 0:
            return posts_list, num_http_requests, num_posts, page_status
        temp_posts_list, collect_posts_further = self.extractEligiblePosts(post_edges, crawl_start_timestamp, crawl_end_timestamp)
        posts_list = posts_list + temp_posts_list
        continue_crawl = has_next_page and collect_posts_further
        while continue_crawl:
            url = self.createPostPageUrl(profile_id, first_limit, cursor, rhx_gis)
            num_http_requests, data = self.getPostPageSource(url, entity, num_http_requests)
            cursor,has_next_page,post_edges = self.parsePostPageJson(data)
            if len(post_edges) == 0:
                return posts_list, num_http_requests, len(posts_list), page_status
            temp_posts_list, collect_posts_further = self.extractEligiblePosts(post_edges, crawl_start_timestamp, crawl_end_timestamp)
            posts_list = posts_list + temp_posts_list
            continue_crawl = has_next_page and collect_posts_further
        return posts_list, num_http_requests, len(posts_list), page_status

    def createFirstPageUrl(self, profile_id):
        return "{}/{}/".format(self.base_url, profile_id)

    def getFirstPageSource(self, frontPageUrl, entity):
        num_http_requests = 0
        continue_get_page_source = True
        get_page_source_loop_count = 0
        while continue_get_page_source:
            if get_page_source_loop_count > 10:
                break
            try:
                profile_page_source = self.session.get(frontPageUrl)
                num_http_requests += 1
                get_page_source_loop_count += 1
                page_status = profile_page_source.status_code
                print("Page Status : "+str(page_status))
                if page_status != 200:
                    if page_status == 404:
                        page_source = 'null'
                        break
                    else:
                        print(entity + " - Got Error Code " + str(page_status) + " So Sleep for " + str(self.WAIT_SECONDS_ON_ERROR) +" seconds")
                        time.sleep(self.WAIT_SECONDS_ON_ERROR)
                        continue
                content = profile_page_source.text
                page_source = re.search('<script[^>]+>window\._sharedData\s+=\s(.*?);<\/script>', content)
                if page_source is None:
                    break
                #with open(self.dump_file.format("{}.json".format(entity+"_posts_page")), 'w') as fp:
                    #fp.write(str(page_source.group(1)))
                continue_get_page_source = False
            except Exception as e:
                print(e)
                print("Front Page source didnt recieved. So sleep")
                time.sleep(self.WAIT_SECONDS_ON_ERROR)
                continue
        return page_source, num_http_requests, page_status

    def parseFirstPageJson(self, first_page_source_json):
        rhx_gis = first_page_source_json.get('rhx_gis')
        if len(first_page_source_json.get('entry_data')) == 0:
            return rhx_gis, len(first_page_source_json.get('entry_data')), 'null', False, 'null'
        profile_id = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('id')
        cursor = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('end_cursor')
        has_next_page = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('has_next_page')
        edges = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('edges')
        return rhx_gis, profile_id, cursor, has_next_page, edges

    def extractEligiblePosts(self, post_edges, crawl_start_timestamp, crawl_end_timestamp):
        collect_posts_further = True
        posts_list = []
        for post in post_edges:
            post_dict = {}
            node = post.get('node')
            post_taken_at_timestamp = node.get('taken_at_timestamp')
            print("post time: "+ str(timezone.make_aware(datetime.fromtimestamp(post_taken_at_timestamp))))
            if crawl_start_timestamp <= post_taken_at_timestamp:
                if post_taken_at_timestamp < crawl_end_timestamp:
                    post_dict["node"] = node
                    posts_list.append(post_dict)
                else:
                    print("Date is less than Reuired range So SKIP this post")
                    continue
            else:
                print("Date is less than Reuired range So not crawling this profile Further")
                collect_posts_further = False
                break
        return posts_list, collect_posts_further

    def get_params(self, profile_id, first_limit, cursor):
        return {
            'id': profile_id,
            'first': first_limit,
            'after': cursor
        }

    def createPostPageUrl(self, profile_id, first_limit, cursor, rhx_gis):
        params = self.get_params(profile_id, first_limit, cursor)
        json_params = json.dumps(params, separators=(',', ':'))
        signature = self.create_gis(rhx_gis, json_params)
        self.session.headers['x-instagram-gis'] = signature
        query_url = "{}{}".format(self.base_url, '/graphql/query/?query_hash={}&variables={{}}'.format('6305d415e36c0a5f0abb6daba312f2dd'))
        url = query_url.format(json_params)
        print(url)
        return url

    def getPostPageSource(self, url, entity, num_http_requests):
        num_http_requests_loop = 0
        while True:
            if num_http_requests_loop > 10:
                break
            try:
                content = self.session.get(url)
                num_http_requests += 1
                num_http_requests_loop += 1
                #with open(self.dump_file.format("{}.json".format(entity+"_posts_page"+str(num_http_requests))), 'w') as fp:
                    #fp.write(content.text)
                data = content.json()
            except Exception as e:
                print(e)
                data = 'null'
                break
            if data.get('status').lower() != 'ok':
                if data.get('message', '') == 'rate limited':
                    print("Rate Limited. Sleeping " + str(self.WAIT_SECONDS_ON_RATE_LIMITED) + ' seconds')
                    time.sleep(self.WAIT_SECONDS_ON_RATE_LIMITED)
                    continue
            break
        return num_http_requests, data

    def parsePostPageJson(self,post_data):
        cursor = post_data.get('data').get('user').get('edge_owner_to_timeline_media').get('page_info').get('end_cursor')
        has_next_page = post_data.get('data').get('user').get('edge_owner_to_timeline_media').get('page_info').get('has_next_page')
        edges = post_data.get('data').get('user').get('edge_owner_to_timeline_media').get('edges')
        return cursor, has_next_page, edges

    def storePostsToDb(self, posts, entity, entity_id):
        def postFromNode(postNode):
            node = postNode.get('node')
            is_video = node.get('is_video')
            if is_video:
                post_type = "Video"
            else:
                post_type = "Picture"
            title = node.get('edge_media_to_caption').get('edges')[0].get('node').get('text') if 'title' in node else None
            post = IgPosts(
                entity_id = entity_id,
                profile_id = entity,
                post_id = node.get('id'),
                post_shortcode = node.get('shortcode'),
                post_url = "https://instagram.com/p/"+node.get('shortcode'),
                #title = title,
                post_datetime = node.get('taken_at_timestamp'),
                status = 'new',
                post_type = post_type
            )
            return post

        posts = [
            postFromNode(postNode) for postNode in posts
        ]
        IgPosts.objects.bulk_create(posts)

    def updateEntitesTable(self, num_http_requests , num_posts, crawl_code, entity_id):
        entity_list = IgEntities.objects.filter(entity_id=entity_id)
        entity = entity_list[0]
        entity.status = 'crawled'
        entity.num_http_reqs = num_http_requests
        entity.num_posts = num_posts
        entity.crawl_code = crawl_code
        entity.crawl_end_time = timezone.now()
        entity.save()
        return
