# Generated by Django 2.0.6 on 2018-07-05 07:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scraper', '0007_auto_20180705_0744'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='entities',
            name='aa_id',
        ),
        migrations.AlterField(
            model_name='entities',
            name='id',
            field=models.IntegerField(primary_key=True, serialize=False),
        ),
    ]
