from scraper.management.commands.scrape_posts import Command
import mock
import re
from requests import Session

posts = Command()
posts.session = posts.init_session(Session())
cursor = 'AQCN_fxEpglxhXjx8mV2WULezDK61_3rcc1stFBtZ6o-J2LLX7g0V53LHF0AKy-ttyyHP_tLi8pCm27Zbn8aih0Wb_A_XCCZm4n6xEZZoHuwaA'
id = 1692051307
rhx_gis = '82093ce5e9ef9a6015a95f7311a00391'
url = 'https://www.instagram.com/graphql/query/?query_hash=6305d415e36c0a5f0abb6daba312f2dd&variables={"after":"AQCN_fxEpglxhXjx8mV2WULezDK61_3rcc1stFBtZ6o-J2LLX7g0V53LHF0AKy-ttyyHP_tLi8pCm27Zbn8aih0Wb_A_XCCZm4n6xEZZoHuwaA","id":1692051307,"first":12}'
def test_create_post_page_url():
    url = posts.createPostPageUrl(id, 12, cursor, rhx_gis)
    #assert posts.createPostPageUrl(id, 12, cursor, rhx_gis) == url

def test_get_post_page_source():
    print(url)
    posts.getPostPageSource(url, 'abc', 1)

def a_test_get_page_source_with_valid_data():
    entity = 'worldstar'
    front_page_url = 'https://www.instagram.com/worldstar/'
    session = posts.init_session(Session())
    profile_page_source = session.get(front_page_url)
    page_status = profile_page_source.status_code
    content = profile_page_source.text
    page_source = re.search('<script[^>]+>window\._sharedData\s+=\s(.*?);<\/script>', content)
    assert posts.getFirstPageSource(front_page_url, entity) == (page_source, 11, page_status)
