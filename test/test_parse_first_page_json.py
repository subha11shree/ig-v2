from scraper.management.commands.scrape_posts import Command
import json

posts = Command()

def test_parse_json_with_valid_data():
    with open('first_page.json', 'r') as file:
        data = file.read()
        first_page_source_json = json.loads(data)

    rhx_gis = first_page_source_json.get('rhx_gis')
    profile_id = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('id')
    cursor = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('end_cursor')
    has_next_page = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('has_next_page')
    edges = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('edges')
    assert posts.parseFirstPageJson(first_page_source_json) == (rhx_gis, profile_id, cursor, has_next_page, edges)

def test_data_has_next_page_false():
    with open('first_page_with_false_has_next_page.json', 'r') as file:
        data = file.read()
        first_page_source_json = json.loads(data)

    rhx_gis = first_page_source_json.get('rhx_gis')
    profile_id = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('id')
    cursor = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('end_cursor')
    has_next_page = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('has_next_page')
    edges = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('edges')
    assert posts.parseFirstPageJson(first_page_source_json) == (rhx_gis, profile_id, cursor, has_next_page, edges)

def test_data_with_empty_edges():
    with open('first_page_with_empty_edges.json', 'r') as file:
        data = file.read()
        first_page_source_json = json.loads(data)

    rhx_gis = first_page_source_json.get('rhx_gis')
    profile_id = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('id')
    cursor = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('end_cursor')
    has_next_page = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('page_info').get('has_next_page')
    edges = first_page_source_json.get('entry_data').get('ProfilePage')[0].get('graphql').get('user').get('edge_owner_to_timeline_media').get('edges')
    assert posts.parseFirstPageJson(first_page_source_json) == (rhx_gis, profile_id, cursor, has_next_page, edges)

def test_data_with_empty_entry_data():
    with open('first_page_with_no_entry_data.json', 'r') as file:
        data = file.read()
        first_page_source_json = json.loads(data)

    rhx_gis = first_page_source_json.get('rhx_gis')
    profile_id = 0
    cursor = 'null'
    has_next_page = False
    edges = 'null'
    assert posts.parseFirstPageJson(first_page_source_json) == (rhx_gis, profile_id, cursor, has_next_page, edges)
