from scraper.management.commands.scrape_posts import Command
import mock
import re
from requests import Session

posts = Command()
posts.session = posts.init_session(Session())


class result(object):
    def __init__(self):
        self.status_code = 500
        self.text = "dvhsgvdhsd"



class custorm(object):
    def get(self, url):
        return result()





def a_test_get_page_source_404():
    entity = 'theoneandonlybifnaked'
    front_page_url = 'https://www.instagram.com/theoneandonlybifnaked/'
    assert posts.getFirstPageSource(front_page_url, entity) == ('null', 1, 404)

def my_get_url():
    return result()

def test_get_page_source_200_none_page_source():
    posts = Command()
    posts.session = custorm()
    entity = 'burnettsvodka'
    front_page_url = 'https://www.instagram.com/burnettsvodka/'

    assert posts.getFirstPageSource(front_page_url, entity) == (None, 1, 200)

def a_test_get_page_source_with_valid_data():
    entity = 'worldstar'
    front_page_url = 'https://www.instagram.com/worldstar/'
    session = posts.init_session(Session())
    profile_page_source = session.get(front_page_url)
    page_status = profile_page_source.status_code
    content = profile_page_source.text
    page_source = re.search('<script[^>]+>window\._sharedData\s+=\s(.*?);<\/script>', content)
    assert posts.getFirstPageSource(front_page_url, entity) == (page_source, 11, page_status)
